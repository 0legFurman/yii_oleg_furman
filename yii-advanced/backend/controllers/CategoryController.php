<?php

namespace backend\controllers;

use app\models\FormImageCategory;
use app\models\FormImageProduct;
use backend\models\FileSystem;
use Yii;
use app\models\Category;
use backend\models\CategorySearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $category = Category::find()->asArray()->where(['parent_id' => 0])->all();

        return $this->render('index', [
            'category' => $category,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search([$searchModel->formName()=>['parent_id' => $id]]);
        $category = Category::find()->asArray()->where(['parent_id' => $id])->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'category' => $category,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('createCategory')){
            throw new ForbiddenHttpException('Access Denied');
        }

        $model = new Category();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->category_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('updateCategory')){
            throw new ForbiddenHttpException('Access Denied');
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->category_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('deleteCategory')){
            throw new ForbiddenHttpException('Access Denied');
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionFormImageCategory($id = 0)
    {
        if (!Yii::$app->user->can('updateCategory')){
            throw new ForbiddenHttpException('Access Denied');
        }

        $model = new FormImageCategory();
        $product = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->setNewImageName();
            if ($model->upload()) {
                $model->oldImageName = $product->image;
                $product->image = $model->newImageName;
                if($product->save()){
                    FileSystem::deleteFile($model->getFolder(). $model->oldImageName);
                    return $this->redirect(['view', 'id' => $id]);
                } else{
                    FileSystem::deleteFile($model->getFolder(). $model->newImageName);
                }
            }
        }

        return $this->render('form_image_category', ['model' => $model]);
    }
}
