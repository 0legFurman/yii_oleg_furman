<?php

namespace backend\controllers;

use app\models\Category;
use app\models\Image;
use backend\models\FileSystem;
use Yii;
use app\models\Product;
use app\models\FormImageProduct;
use backend\models\ProductSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    protected $category;
    protected $subCat;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex($id = null, $cat = null)
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if ($id != null && $cat != null) {
            $product = Product::find()->where(['category_id' => $id])->asArray()->all();
            $this->category = Category::find()->where(['category_id' => $id])->one();
            $this->subCat = Category::find()->where(['category_id' => $cat])->one();
        }
        else{
            $product = Product::find()->all();
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'product' => $product,
            'category' => $this->category,
            'subcat' => $this->subCat,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'category' => $this->category,
            'subcat' => $this->subCat,
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        if (!Yii::$app->user->can('createProduct')){
            throw new ForbiddenHttpException('Access Denied');
        }


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->product_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('updateProduct', ['product' => $model])){
            throw new ForbiddenHttpException('Access Denied');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->product_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('deleteProduct')){
            throw new ForbiddenHttpException('Access Denied');
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionFormImageProduct($id = 0)
    {
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('updateProduct', ['product' => $model])){
            throw new ForbiddenHttpException('Access Denied');
        }

        $model = new FormImageProduct();
        $product = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->setNewImageName();
            if ($model->upload()) {
                $model->oldImageName = $product->image;
                $product->image = $model->newImageName;
                if($product->save()){
                    FileSystem::deleteFile($model->getFolder(). $model->oldImageName);
                    return $this->redirect(['view', 'id' => $id]);
                } else{
                    FileSystem::deleteFile($model->getFolder(). $model->newImageName);
                }
            }
        }

        return $this->render('form_image_product', ['model' => $model]);
    }

    protected function getBreadCrumbs($id)
    {

    }
}
