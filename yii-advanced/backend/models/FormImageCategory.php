<?php


namespace app\models;


use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\imagine\Image;
use yii\web\UploadedFile;

class FormImageCategory extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $newImageName;
    public $oldImageName;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function setNewImageName()
    {
        $this->newImageName = Yii::$app->security->generateRandomString(). '.' .$this->imageFile->extension;
    }

    public function getFolder()
    {
        return Yii::getAlias('@frontend'). '/web/images/category/';
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->imageFile->saveAs($this->getFolder() . $this->newImageName);
            Image::thumbnail($this->getFolder() . $this->newImageName, 800, null)
                ->save($this->getFolder() . 'res'.$this->newImageName,
                    ['quality' => 80]);
            unlink($this->getFolder() . $this->newImageName);
            $this->newImageName = 'res'.$this->newImageName;
            return true;
        } else {
            return false;
        }
    }
}