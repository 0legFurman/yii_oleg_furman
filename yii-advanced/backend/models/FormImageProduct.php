<?php

namespace app\models;

use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\Box;


class FormImageProduct extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $newImageName;
    public $oldImageName;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function setNewImageName()
    {
        $this->newImageName = Yii::$app->security->generateRandomString(). '.' .$this->imageFile->extension;
    }

    public function getFolder()
    {
        return Yii::getAlias('@frontend'). '/web/images/product/';
    }

    public function upload()
    {

        if ($this->validate()) {
            $this->imageFile->saveAs($this->getFolder() . $this->newImageName);
            Image::thumbnail($this->getFolder() . $this->newImageName, 800, 600, $mode = ManipulatorInterface::THUMBNAIL_INSET)
                ->save($this->getFolder() . 'upd'.$this->newImageName,
                    ['quality' => 80]);
            unlink($this->getFolder() . $this->newImageName);
            $this->newImageName = 'upd'.$this->newImageName;
            return true;
        } else {
            return false;
        }
    }
}