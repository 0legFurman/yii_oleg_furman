<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "manufacturer".
 *
 * @property int $manufacturer_id
 * @property string $name
 * @property string $image
 *
 * @property Product[] $products
 */
class Manufacturer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manufacturer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'image'], 'required'],
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'manufacturer_id' => 'Manufacturer ID',
            'name' => 'Name',
            'image' => 'Image',
        ];
    }

    /**
     * Gets query for [[Products]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['manufacturer_id' => 'manufacturer_id']);
    }

    public function getImage()
    {
        if (empty($this->image) || !is_file(Yii::getAlias('@frontend'). '/web/images/manufactures/' .$this->image)){
            return '/frontend/web/images/image-placeholder-350x350.png';
        }
        return '/frontend/web/images/manufactures/' . $this->image;
    }

    public function getNewImage()
    {
        return Image::Resize($this->getImage());
    }
}
