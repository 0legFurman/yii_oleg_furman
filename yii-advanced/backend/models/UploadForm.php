<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $newImageName;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function setNewImageName()
    {
        $this->newImageName = Yii::$app->security->generateRandomString(). '.' .$this->imageFile->extension;
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->imageFile->saveAs(Yii::getAlias('@frontend'). '/web/images.product/' . $this->newImageName);
            return true;
        } else {
            return false;
        }
    }
}