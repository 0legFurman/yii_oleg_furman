<?php
namespace console\controllers;

use common\rbac\AuthorRule;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $viewCategory = $auth->createPermission('viewCategory');
        $auth->add($viewCategory);

        $viewProduct = $auth->createPermission('viewProduct');
        $auth->add($viewProduct);

        $viewManufacturer = $auth->createPermission('viewManufacturer');
        $auth->add($viewManufacturer);

        $createProduct = $auth->createPermission('createProduct');
        $auth->add($createProduct);

        $updateProduct = $auth->createPermission('updateProduct');
        $auth->add($updateProduct);

        $createCategory = $auth->createPermission('createCategory');
        $auth->add($createCategory);

        $updateCategory = $auth->createPermission('updateCategory');
        $auth->add($updateCategory);

        $createManufacturer = $auth->createPermission('createManufacturer');
        $auth->add($createManufacturer);

        $updateManufacturer = $auth->createPermission('updateManufacturer');
        $auth->add($updateManufacturer);

        $deleteProduct = $auth->createPermission('deleteProduct');
        $auth->add($deleteProduct);

        $deleteCategory = $auth->createPermission('deleteCategory');
        $auth->add($deleteCategory);

        $deleteManufacturer = $auth->createPermission('deleteManufacturer');
        $auth->add($deleteManufacturer);


        // добавляем разрешение "createPost"
        $createPost = $auth->createPermission('createPost');
        $createPost->description = 'Create a post';
        $auth->add($createPost);

        // добавляем разрешение "updatePost"
        $updatePost = $auth->createPermission('updatePost');
        $updatePost->description = 'Update post';
        $auth->add($updatePost);

        $guest = $auth->createRole('guest');
        $auth->add($guest);
        $auth->addChild($guest, $viewProduct);
        $auth->addChild($guest, $viewCategory);

        $authUser = $auth->createRole('authUser');
        $auth->add($authUser);
        $auth->addChild($authUser, $guest);
        $auth->addChild($authUser, $viewManufacturer);

        $manager = $auth->createRole('manager');
        $auth->add($manager);
        $auth->addChild($manager, $authUser);
        $auth->addChild($manager, $createProduct);


        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $manager);
        $auth->addChild($admin, $createCategory);
        $auth->addChild($admin, $updateCategory);
        $auth->addChild($admin, $updateProduct);
        $auth->addChild($admin, $createManufacturer);
        $auth->addChild($admin, $updateManufacturer);

        $superAdmin = $auth->createRole('superAdmin');
        $auth->add($superAdmin);
        $auth->addChild($superAdmin, $admin);
        $auth->addChild($superAdmin, $deleteProduct);
        $auth->addChild($superAdmin, $deleteCategory);
        $auth->addChild($superAdmin, $deleteManufacturer);

        // Назначение ролей пользователям. 1 и 2 это IDs возвращаемые IdentityInterface::getId()
        // обычно реализуемый в модели User.
        //$auth->assign($author, 2);
        //$auth->assign($admin, 1);
    }

    public function actionCreateRule()
    {
        $auth = Yii::$app->authManager;

// add the rule
        $rule = new AuthorRule();
        $auth->add($rule);

// добавляем разрешение "updateOwnPost" и привязываем к нему правило.
        $updateOwnProduct = $auth->createPermission('updateOwnProduct');
        $updateOwnProduct->description = 'Update own product';
        $updateOwnProduct->ruleName = $rule->name;
        $auth->add($updateOwnProduct);

        $updateProduct = $auth->getPermission('updateProduct');
        $manager = $auth->getRole('manager');

// "updateOwnPost" будет использоваться из "updatePost"
        $auth->addChild($updateOwnProduct, $updateProduct);

// разрешаем "автору" обновлять его посты
        $auth->addChild($manager, $updateOwnProduct);
    }
}