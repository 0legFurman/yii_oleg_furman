<?php


namespace app\controllers;


use app\models\Country;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\Continent;

class ContinentController extends Controller
{
    public function actionIndex()
    {
        $continents = Continent::find()->asArray()->all();
        return $this->render('index', ['continents' => $continents]);
    }

    public function actionView($code = "AF")
    {
        $continent = Continent::find()->where(['code'=>$code])->one();

        $countriesDataProvider = new ActiveDataProvider([
           'query' => Country::find()->where(['continent_id'=>$continent->continent_id]),
           'pagination'=>[
               'pageSize' => 20,
           ]
        ]);

        return $this->render('view', compact('continent', 'countriesDataProvider'));
    }
}