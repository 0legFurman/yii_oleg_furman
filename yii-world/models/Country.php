<?php


namespace app\models;



use yii\db\ActiveRecord;

class Country extends ActiveRecord
{
    public function getContinent()
    {
        return $this->hasOne(Continent::className(), ['continent_id'=>'continent_id']);
    }
}