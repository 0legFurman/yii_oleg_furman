<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>

<?php
$this->params['breadcrumbs'][] = ['label' => 'Continents', 'url' =>['continent/index']];
?>

<div class="container">
    <div class="row">
    <?php foreach ($continents as $continentItem): ?>
    <div class="col-md-4">
        <div class="panel panel-default" style="min-height: 500px">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a href="<?= Url::to(['continent/view', 'code' => $continentItem['code']]); ?>" class="list-group-item">
                    <?=Html::encode($continentItem['name']) ?>
                    </a>
                    <?=Html::img('@web/images/continents/'. strtolower($continentItem['code']). '.png', ['alt' => 'My logo', 'width'=>'100%']) ?>
                </h3>
            </div>
            <div class="panel-body">
                <?=Html::encode($continentItem['description']) ?>
            </div>
        </div>
    </div>
    <? endforeach; ?>
</div>
</div>
