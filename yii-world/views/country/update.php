<?php

use yii\helpers\Html;
use app\models\Continent;


/* @var $this yii\web\View */
/* @var $model app\models\Country */

$this->title = 'Update Country: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->country_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="country-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <? $continent = Continent::find()->all(); ?>
    <?= $this->render('_form', [
        'model' => $model, 'continent' => $continent
    ]) ?>

</div>
