<?php

use yii\widgets\DetailView;
use yii\helpers\Url;
?>

<?php
$this->params['breadcrumbs'][] = ['label' => 'Continents', 'url' =>['continent/index']];
$this->params['breadcrumbs'][] = ['label' => $model->continent->name, 'url' =>['continent/view', 'code'=>$model->continent->code]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' =>['country/view', 'id'=>$model->country_id]];
?>
<a class="btn btn-primary" href="<?=Url::to(['country/update', 'id'=>$model->country_id]); ?>" role="button">Update</a>
<div class="container">
    <div class="row">
        <div class="col-12 col-md-6">
            <?php
            echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'official_name',
                    'currency',
                    'area',
                    'capital',
                    [
                        'attribute'=>'flag',
                        'value'=>'@web/images/countries/png250px/' . strtoLower($model->code) . '.png',
                        'format' => ['image']
                    ]
                ],
            ]);
            ?>
        </div>
        <div class="col-12 col-md-6">
            <script
                    src="https://maps.googleapis.com/maps/api/js?&callback=initMap&libraries=&v=weekly&sensor=false"
                    defer
            ></script>
            <style type="text/css">
                /* Set the size of the div element that contains the map */
                #map {
                    height: 400px;
                    /* The height is 400 pixels */
                    width: 100%;
                    /* The width is the width of the web page */
                }
            </style>
            <script>
                // Initialize and add the map
                function initMap() {
                    // The location of Uluru
                    const uluru = { lat: -25.344, lng: 131.036 };
                    // The map, centered at Uluru
                    const map = new google.maps.Map(document.getElementById("map"), {
                        zoom: 4,
                        center: uluru,
                    });
                    // The marker, positioned at Uluru
                    const marker = new google.maps.Marker({
                        position: uluru,
                        map: map,
                    });
                }
            </script>
            <div id="map"></div>
        </div>
    </div>
</div>

