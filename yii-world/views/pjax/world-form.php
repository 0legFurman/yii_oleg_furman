<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
?>

<?php Pjax::begin([
    'id' => 'world-form-container'
]); ?>
<?php $form = ActiveForm::begin([
    'options' => ['data' => ['pjax' => true],],
    'id' => 'world-form'
]); ?>

<?= $form->field($model, 'continent_id')->dropDownList(ArrayHelper::map($continents, 'continent_id', 'name'), [
    'id' => 'field-continent-id',
    'onchange' => '$("#world-form").submit()'
]) ?>
    <h2><?= $continent->name ?></h2>
    <p><?= $continent->description ?></p>
<?php if ($countries != null) :?>
<?= $form->field($model, 'country_id')->dropDownList(ArrayHelper::map($countries, 'country_id', 'name'), [
    'id' => 'field-country-id',
    'onchange' => '$("#world-form").submit()'
]) ?>
    <h2><?= $country->name ?></h2>
    <p><?= $country->code ?></p>
<?php endif; ?>
<?php if ($regions != null) :?>
    <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map($regions, 'region_id', 'name_language'), [
        'id' => 'field-region-id',
        'onchange' => '$("#world-form").submit()'
    ]) ?>
    <h2><?= $region->name_language ?></h2>
<?php endif; ?>
<?php if ($cities != null) :?>
    <?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map($cities, 'city_id', 'name_language'), [
        'id' => 'field-city-id',
        'onchange' => '$("#world-form").submit()'
    ]) ?>
<?php endif; ?>
    <?php if (isset($data->name)) :?>
    <h2>Погода в місті <?= $city->name_language ?></h2>
<p>Температура: <?=$data->main->temp?></p>
<p>Вологість: <?=$data->main->humidity?></p>
<p>Тиск: <?=$data->main->pressure?></p>
<?php endif; ?>
<?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>